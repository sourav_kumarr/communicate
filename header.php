<?php
session_start();
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Communication</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/heroic-features.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.css"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>
<!-- Bootstrap Core JavaScript -->
<header>
    <div class="col-md-12 headerbox nogutter">
        <div class="col-xs-9  col-md-2 nogutter logo_box">
            <a href="index.php">
                <img src="images/logo.png" class="img-responsive logo"/>
            </a>
        </div>
        <div class="col-xs-3 col-md-1 pull-right right_nav_bars">
            <!-- for md device nav-bar start here -->
            <div id="nav-icon1">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <!-- for md device nav-bar end here -->
            <!-- for less md device nav-bar start here here -->
            <div id="nav-icon2">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <!-- for less md device nav-bar end here here -->
        </div>
        <div class="col-xs-12 col-md-2 nogutter social_icons" id="sicons" style="">
            <div class="row social-icons-row">
                <?php
                if(isset($_SESSION['comm_email_id']))
                {
                ?>
                    <a title="Logout" href="logout.php" >
                        <i class="fa fa-power-off social" style="color: #D6005C"></i>
                    </a>
                 <?php
                }
                ?>

                <a title="Instagram" href="https://www.instagram.com/sachtech/" target="_blank">
                    <i class="fa fa-instagram social" style="color: #D6005C"></i>
                </a>
                <a href="https://www.youtube.com/stsmentor" data-toggle="tooltip" data-placement="top" target="_blank">
                    <i title="YouTube" class="fa fa-youtube social" style="color: #E52C27"></i>
                </a>
                <a href="https://www.linkedin.com/in/sachtech-solution-private-limited-ab5b5bb7/" target="_blank">
                    <i title="LinkedIn" class="fa fa-linkedin social" style="color: #0077B5"></i>
                </a>
                <a href="https://plus.google.com/108110002229769330623" target="_blank">
                    <i title="Google Plus" class="fa fa-google-plus social" style="color: #DC4937"></i>
                </a>
                <a href="https://twitter.com/sach_tech" target="_blank">
                    <i title="Twitter" class="fa fa-twitter social" style="color: #32CCFE"></i>
                </a>
                <a href="https://www.facebook.com/search/top/?q=SachTech%20Solution%20Private%20Limited"
                   target="_blank">
                    <i title="Facebook" class="fa fa-facebook social" style="color: #3C5A98"></i>
                </a>
            </div>
        </div>
        <div class="col-md-7 nogutter main-menu" id="main-menu" style="">
            <div class="row main-menu-large">
                <div class="col-md-12" style="font-size: 26px;margin-top: 3%;text-align: center"><label style="color: #ab1522">IoT</label> <span style="color: #ab1522">(Internet Of Things)</span></div>
                <!--<div class="col-md-2">
                <ul class="menubox">
                    <?php
/*
                    $comm_useremail ="";
                    if(isset($_SESSION['comm_email_id']))
                    {
                       */?>
                        <a href="logout.php" class="header-ancor">
                            <li class="menuitem">Logout</li>
                        </a>
                        <?php
/*                    }
                    else
                    {
                       */?>
                        <a href="signin.php" class="header-ancor">
                            <li class="menuitem">Login</li>
                        </a>
                        <?php
/*                    }
                    */?>

                </ul>
                </div>-->
            </div>
            <!--<div class="row main-menu-small">
                <ul class="small-menu-ul small-menu-ul-active">
                    
                    <?php
/*                    $comm_useremail ="";
                    if(isset($_SESSION['comm_email_id']))
                    {
                        */?>
                        <a href="logout.php" class="header-ancor">
                            <li class="menuitem">Logout</li>
                        </a>
                        <?php
/*                    }
                    else
                    {
                        */?>
                        <a href="signin.php" class="header-ancor">
                            <li class="menuitem">Login</li>
                        </a>
                        <?php
/*                    }
                    */?>
                </ul>
            </div>-->
        </div>
    </div>
    <div class="clear"></div>
    <div class="col-md-12 strip"></div>
</header>
<body class="" onload="loadmenu()">
<!-- Navigation -->
<!--<nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #428bca;color: whitesmoke" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php" >Communication</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li id="home" class="active">
                    <a href="index.php">Home</a>
                </li>
                <li id="setting">
                    <a href="setting.php">Setting</a>
                </li>
                <li id="services">
                    <a href="#">Services</a>
                </li>
                <li id="contact">
                    <a href="#">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>-->
<!--
<div class="container">-->

<div id="login_modal" class="modal"   aria-hidden="true" data-backdrop="false">
    <!-- Modal content -->
    <div class="modal-dialog modal-mg">
        <div class="modal-content" style="width: 100%;">
            <h4 class="modal-title modal_titles">Login</h4>
            <hr>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

