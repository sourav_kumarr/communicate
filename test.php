<?php
include 'header.php';
?>
<style>
    .container {
        margin: auto;
        width:100%;
        padding-left: 121px !important;
        padding-right: 130px !important;
    }
    .power {
        cursor: pointer;
    }
    #on {
        -webkit-transform: translate(50%, 50%) scale(0);
        transform: translate(50%, 50%) scale(0);
        opacity: 0;
    }
    .active #on {
        opacity: 1;
        -webkit-transform: translate(0) scale(1);
        transform: translate(0) scale(1);
        -webkit-transition: all 0.14s ease-in;
        transition: all 0.14s ease-in;
    }

    .fancy-bulb {
        position: relative;
    }

    .streaks, .streaks:after, .streaks:before {
        position: absolute;
        background: #ff2600;
        border-radius: 5.5px;
        height: 11px;
    }

    .streaks:after, .streaks:before {
        content: "";
        display: block;
    }

    .streaks:before {
        bottom: 65px;
    }

    .streaks:after {
        top: 65px;
    }

    .left-streaks {
        right: 217px;
        top: 89.5px;
    }
    .active .left-streaks {
        -webkit-animation: move-left 0.38s ease-out, width-to-zero 0.38s ease-out;
        animation: move-left 0.38s ease-out, width-to-zero 0.38s ease-out;
        -webkit-animation-delay: 0.14s;
        animation-delay: 0.14s;
    }
    .left-streaks:before, .left-streaks:after {
        left: 15px;
    }
    .active .left-streaks:before {
        -webkit-animation: width-to-zero 0.38s ease-out, move-up 0.38s ease-out;
        animation: width-to-zero 0.38s ease-out, move-up 0.38s ease-out;
        -webkit-animation-delay: 0.14s;
        animation-delay: 0.14s;
    }
    .active .left-streaks:after {
        -webkit-animation: width-to-zero 0.38s ease-out, move-down 0.38s ease-out;
        animation: width-to-zero 0.38s ease-out, move-down 0.38s ease-out;
        -webkit-animation-delay: 0.14s;
        animation-delay: 0.14s;
    }

    .right-streaks {
        left: 217px;
        top: 89.5px;
    }
    .active .right-streaks {
        -webkit-animation: move-right 0.38s ease-out, width-to-zero 0.38s ease-out;
        animation: move-right 0.38s ease-out, width-to-zero 0.38s ease-out;
        -webkit-animation-delay: 0.14s;
        animation-delay: 0.14s;
    }
    .right-streaks:before, .right-streaks:after {
        right: 15px;
    }
    .active .right-streaks:before {
        -webkit-animation: width-to-zero 0.38s ease-out, move-up 0.38s ease-out;
        animation: width-to-zero 0.38s ease-out, move-up 0.38s ease-out;
        -webkit-animation-delay: 0.14s;
        animation-delay: 0.14s;
    }
    .active .right-streaks:after {
        -webkit-animation: width-to-zero 0.38s ease-out, move-down 0.38s ease-out;
        animation: width-to-zero 0.38s ease-out, move-down 0.38s ease-out;
        -webkit-animation-delay: 0.14s;
        animation-delay: 0.14s;
    }

    .left-streaks:before, .right-streaks:after {
        -webkit-transform: rotate(34deg);
        transform: rotate(34deg);
    }

    .left-streaks:after, .right-streaks:before {
        -webkit-transform: rotate(-34deg);
        transform: rotate(-34deg);
    }

    @-webkit-keyframes move-left {
        0% {
            -webkit-transform: none;
            transform: none;
        }
        65% {
            -webkit-transform: translateX(-80px);
            transform: translateX(-80px);
        }
        100% {
            -webkit-transform: translateX(-80px);
            transform: translateX(-80px);
        }
    }

    @keyframes move-left {
        0% {
            -webkit-transform: none;
            transform: none;
        }
        65% {
            -webkit-transform: translateX(-80px);
            transform: translateX(-80px);
        }
        100% {
            -webkit-transform: translateX(-80px);
            transform: translateX(-80px);
        }
    }
    @-webkit-keyframes move-right {
        0% {
            -webkit-transform: none;
            transform: none;
        }
        65% {
            -webkit-transform: translateX(80px);
            transform: translateX(80px);
        }
        100% {
            -webkit-transform: translateX(80px);
            transform: translateX(80px);
        }
    }
    @keyframes move-right {
        0% {
            -webkit-transform: none;
            transform: none;
        }
        65% {
            -webkit-transform: translateX(80px);
            transform: translateX(80px);
        }
        100% {
            -webkit-transform: translateX(80px);
            transform: translateX(80px);
        }
    }
    @-webkit-keyframes width-to-zero {
        0% {
            width: 50px;
        }
        100% {
            width: 11px;
        }
    }
    @keyframes width-to-zero {
        0% {
            width: 50px;
        }
        100% {
            width: 11px;
        }
    }
    @-webkit-keyframes move-up {
        100% {
            bottom: 100.75px;
        }
    }
    @keyframes move-up {
        100% {
            bottom: 100.75px;
        }
    }
    @-webkit-keyframes move-down {
        100% {
            top: 100.75px;
        }
    }
    @keyframes move-down {
        100% {
            top: 100.75px;
        }
    }
</style>
<div class="row text-center">
    <div class="col-md-12 main-content">
        <div class="col-md-12">
            <label class="temprature">Temperature : <span id="temp_value">0</span> &#x2103;</label>
        </div>
        <div class="col-md-6 col-sm-6 hero-feature">
            <div class="col-md-12 led thumbnail" id="led_1">
                <div class="power" onclick="glow('led_11')" id="led_11">
                    <div class="container">
                        <div class="fancy-bulb">
                            <div class="left-streaks streaks"></div>
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 275.3 413.3" enable-background="new 0 0 275.3 413.3" xml:space="preserve">
<g id="off">
    <path fill="#E2ECF1" d="M137.7,13.7C67.2,13.7,10,70.9,10,141.4c0,58.3,72.8,118.2,79.9,162.3h47.8h47.8
		c7.1-44,79.9-103.9,79.9-162.3C265.3,70.9,208.2,13.7,137.7,13.7z"/>
</g>
                                <g id="on">
                                    <path fill="#ff2600" d="M137.7,13.7C67.2,13.7,10,70.9,10,141.4c0,58.3,72.8,118.2,79.9,162.3h47.8h47.8
		c7.1-44,79.9-103.9,79.9-162.3C265.3,70.9,208.2,13.7,137.7,13.7z"/>

                                </g>

                                <g id="outline">
                                    <path fill="#F1F2F2" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" d="M168.5,375.5h-61.7c-8.9,0-16-7.2-16-16
		v-55.8h93.8v55.8C184.6,368.3,177.4,375.5,168.5,375.5z"/>
                                    <path fill="#F1F2F2" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" d="M151.2,401.5h-27.1c-3.9,0-7-3.2-7-7v-19
		h41.1v19C158.2,398.4,155.1,401.5,151.2,401.5z"/>
                                    <line fill="none" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" x1="184.6" y1="339.6" x2="90.8" y2="339.6"/>
                                    <path fill="none" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" d="M137.7,13.7C67.2,13.7,10,70.9,10,141.4
		c0,58.3,72.8,118.2,79.9,162.3h47.8h47.8c7.1-44,79.9-103.9,79.9-162.3C265.3,70.9,208.2,13.7,137.7,13.7z"/>
                                </g>
                                <g id="highlight">
                                    <path fill="#ff2600" stroke="#FFFFFF" stroke-width="21.0124" stroke-linecap="round" stroke-miterlimit="10" d="M207.1,89.5
		c-12.3-16.1-28.4-29.1-46.9-37.8"/>
                                    <path fill="#ff2600" stroke="#FFFFFF" stroke-width="21.0124" stroke-linecap="round" stroke-miterlimit="10" d="M225,121.4
		c-0.8-2.2-1.8-4.4-2.7-6.5"/>
                                </g>
</svg>
                            <div class="right-streaks streaks"></div>
                        </div>
                    </div>
                </div>
                    <h3>LED 1</h3>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 hero-feature">
            <div class="col-md-12 led thumbnail" id="led_2">
                <div class="power" onclick="glow('led_22')" id="led_22">
                    <div class="container">
                        <div class="fancy-bulb">
                            <div class="left-streaks streaks"></div>
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 275.3 413.3" enable-background="new 0 0 275.3 413.3" xml:space="preserve">
<g id="off">
    <path fill="#E2ECF1" d="M137.7,13.7C67.2,13.7,10,70.9,10,141.4c0,58.3,72.8,118.2,79.9,162.3h47.8h47.8
		c7.1-44,79.9-103.9,79.9-162.3C265.3,70.9,208.2,13.7,137.7,13.7z"/>
</g>
                                <g id="on">
                                    <path fill="#ff2600" d="M137.7,13.7C67.2,13.7,10,70.9,10,141.4c0,58.3,72.8,118.2,79.9,162.3h47.8h47.8
		c7.1-44,79.9-103.9,79.9-162.3C265.3,70.9,208.2,13.7,137.7,13.7z"/>

                                </g>

                                <g id="outline">
                                    <path fill="#F1F2F2" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" d="M168.5,375.5h-61.7c-8.9,0-16-7.2-16-16
		v-55.8h93.8v55.8C184.6,368.3,177.4,375.5,168.5,375.5z"/>
                                    <path fill="#F1F2F2" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" d="M151.2,401.5h-27.1c-3.9,0-7-3.2-7-7v-19
		h41.1v19C158.2,398.4,155.1,401.5,151.2,401.5z"/>
                                    <line fill="none" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" x1="184.6" y1="339.6" x2="90.8" y2="339.6"/>
                                    <path fill="none" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" d="M137.7,13.7C67.2,13.7,10,70.9,10,141.4
		c0,58.3,72.8,118.2,79.9,162.3h47.8h47.8c7.1-44,79.9-103.9,79.9-162.3C265.3,70.9,208.2,13.7,137.7,13.7z"/>
                                </g>
                                <g id="highlight">
                                    <path fill="#ff2600" stroke="#FFFFFF" stroke-width="21.0124" stroke-linecap="round" stroke-miterlimit="10" d="M207.1,89.5
		c-12.3-16.1-28.4-29.1-46.9-37.8"/>
                                    <path fill="#ff2600" stroke="#FFFFFF" stroke-width="21.0124" stroke-linecap="round" stroke-miterlimit="10" d="M225,121.4
		c-0.8-2.2-1.8-4.4-2.7-6.5"/>
                                </g>
</svg>
                            <div class="right-streaks streaks"></div>
                        </div>
                    </div>
                </div>
                <h3>LED 2</h3>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 hero-feature">
            <div class="col-md-12 led thumbnail" id="led_3">
                <div class="power" onclick="glow('led_33')"  id="led_33">
                    <div class="container">
                        <div class="fancy-bulb">
                            <div class="left-streaks streaks"></div>
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 275.3 413.3" enable-background="new 0 0 275.3 413.3" xml:space="preserve">
<g id="off">
    <path fill="#E2ECF1" d="M137.7,13.7C67.2,13.7,10,70.9,10,141.4c0,58.3,72.8,118.2,79.9,162.3h47.8h47.8
		c7.1-44,79.9-103.9,79.9-162.3C265.3,70.9,208.2,13.7,137.7,13.7z"/>
</g>
                                <g id="on">
                                    <path fill="#ff2600" d="M137.7,13.7C67.2,13.7,10,70.9,10,141.4c0,58.3,72.8,118.2,79.9,162.3h47.8h47.8
		c7.1-44,79.9-103.9,79.9-162.3C265.3,70.9,208.2,13.7,137.7,13.7z"/>

                                </g>

                                <g id="outline">
                                    <path fill="#F1F2F2" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" d="M168.5,375.5h-61.7c-8.9,0-16-7.2-16-16
		v-55.8h93.8v55.8C184.6,368.3,177.4,375.5,168.5,375.5z"/>
                                    <path fill="#F1F2F2" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" d="M151.2,401.5h-27.1c-3.9,0-7-3.2-7-7v-19
		h41.1v19C158.2,398.4,155.1,401.5,151.2,401.5z"/>
                                    <line fill="none" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" x1="184.6" y1="339.6" x2="90.8" y2="339.6"/>
                                    <path fill="none" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" d="M137.7,13.7C67.2,13.7,10,70.9,10,141.4
		c0,58.3,72.8,118.2,79.9,162.3h47.8h47.8c7.1-44,79.9-103.9,79.9-162.3C265.3,70.9,208.2,13.7,137.7,13.7z"/>
                                </g>
                                <g id="highlight">
                                    <path fill="#ff2600" stroke="#FFFFFF" stroke-width="21.0124" stroke-linecap="round" stroke-miterlimit="10" d="M207.1,89.5
		c-12.3-16.1-28.4-29.1-46.9-37.8"/>
                                    <path fill="#ff2600" stroke="#FFFFFF" stroke-width="21.0124" stroke-linecap="round" stroke-miterlimit="10" d="M225,121.4
		c-0.8-2.2-1.8-4.4-2.7-6.5"/>
                                </g>
</svg>
                            <div class="right-streaks streaks"></div>
                        </div>
                    </div>
                </div>
                <h3>LED 3</h3>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 hero-feature">
            <div class="col-md-12 led thumbnail" id="led_4">
                <div class="power" onclick="glow('led_44')"  id="led_44">
                    <div class="container">
                        <div class="fancy-bulb">
                            <div class="left-streaks streaks"></div>
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 275.3 413.3" enable-background="new 0 0 275.3 413.3" xml:space="preserve">
<g id="off">
    <path fill="#E2ECF1" d="M137.7,13.7C67.2,13.7,10,70.9,10,141.4c0,58.3,72.8,118.2,79.9,162.3h47.8h47.8
		c7.1-44,79.9-103.9,79.9-162.3C265.3,70.9,208.2,13.7,137.7,13.7z"/>
</g>
                                <g id="on">
                                    <path fill="#ff2600" d="M137.7,13.7C67.2,13.7,10,70.9,10,141.4c0,58.3,72.8,118.2,79.9,162.3h47.8h47.8
		c7.1-44,79.9-103.9,79.9-162.3C265.3,70.9,208.2,13.7,137.7,13.7z"/>

                                </g>

                                <g id="outline">
                                    <path fill="#F1F2F2" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" d="M168.5,375.5h-61.7c-8.9,0-16-7.2-16-16
		v-55.8h93.8v55.8C184.6,368.3,177.4,375.5,168.5,375.5z"/>
                                    <path fill="#F1F2F2" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" d="M151.2,401.5h-27.1c-3.9,0-7-3.2-7-7v-19
		h41.1v19C158.2,398.4,155.1,401.5,151.2,401.5z"/>
                                    <line fill="none" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" x1="184.6" y1="339.6" x2="90.8" y2="339.6"/>
                                    <path fill="none" stroke="#38434A" stroke-width="19.1022" stroke-miterlimit="10" d="M137.7,13.7C67.2,13.7,10,70.9,10,141.4
		c0,58.3,72.8,118.2,79.9,162.3h47.8h47.8c7.1-44,79.9-103.9,79.9-162.3C265.3,70.9,208.2,13.7,137.7,13.7z"/>
                                </g>
                                <g id="highlight">
                                    <path fill="#ff2600" stroke="#FFFFFF" stroke-width="21.0124" stroke-linecap="round" stroke-miterlimit="10" d="M207.1,89.5
		c-12.3-16.1-28.4-29.1-46.9-37.8"/>
                                    <path fill="#ff2600" stroke="#FFFFFF" stroke-width="21.0124" stroke-linecap="round" stroke-miterlimit="10" d="M225,121.4
		c-0.8-2.2-1.8-4.4-2.7-6.5"/>
                                </g>
</svg>
                            <div class="right-streaks streaks"></div>
                        </div>
                    </div>
                </div>
                <h3>LED 4</h3>
            </div>
        </div>
    </div>
    </div>
    <div class="col-md-12" style="text-align: center">
        <label class="acload" >ACLOAD</label>
    </div>

<?php include 'footer.php';?>