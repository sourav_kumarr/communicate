
<!-- Footer -->
<!--</div>-->

<footer>

    <div class="col-xs-12 col-md-12 footer-main">
        <div class="row footer-main-1">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="col-xs-12 col-md-7">
                    <div class="row">
                        <a href="contact">
                            <h1 class="footer-contact"> CONTACT US</h1>
                        </a>
                    </div>
                    <div class="row">
                        <p class="footer-p-text">
                            We believe in Quality and SachTech Solution is registered trademarks of SachTech Solution Pvt. Ltd.
                        </p>
                        <p class="footer-p-text">All rights reserved by sachtechsolution.com</p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-5 footer-address-box1">
                    <div class="row footer-comp-box">
                        <p class="footer-comp-text"> #E-110 Industrial Area, Phase 7 Mohali, India </p>
                        <p class="footer-comp-text"> +91-7087425488 </p>
                        <p class="footer-comp-text"> +91-7087972568 </p>
                        <p class="footer-comp-text"> info@sachtechsolution.com </p>
                        <p class="footer-comp-text"> http://sachtechsolution.com </p>

                        <div class="social_icons_footer">
                            <a href="https://www.facebook.com/search/top/?q=SachTech%20Solution%20Private%20Limited"
                               target="_blank" class="textDeco_no">
                                <i title="Facebook" class="fa fa-facebook social_footer" style="color: #3C5A98"></i>
                            </a>

                            <a href="https://twitter.com/sach_tech" target="_blank">
                                <i title="Twitter" class="fa fa-twitter social_footer" style="color: #32CCFE"></i>
                            </a>
                            <a href="https://plus.google.com/108110002229769330623" target="_blank">
                                <i title="Google Plus" class="fa fa-google-plus social_footer" style="color: #DC4937"></i>
                            </a>
                            <!--<a href="https://stsmentor7@gmail.com" target="_blank">
                                <i title="Skype" class="fa fa-skype social_footer" style="color: #00AFF0"></i>
                            </a>-->
                            <a href="https://www.linkedin.com/in/sachtech-solution-private-limited-ab5b5bb7/"
                               target="_blank">
                                <i title="LinkedIn" class="fa fa-linkedin social_footer" style="color: #0077B5"></i>
                            </a>
                            <a href="https://www.youtube.com/stsmentor" target="_blank">
                                <i title="YouTube" class="fa fa-youtube social_footer" style="color: #E52C27"></i>
                            </a>
                            <a href="https://www.instagram.com/sachtech/" target="_blank">
                                <i title="Instagram" class="fa fa-instagram social_footer" style="color: #D6005C"></i>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>

</footer>
<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script><!--
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>-->

<script src="js/bootstrap.min.js"></script>
<script src="js/db.js"></script>
<script src="js/commands.js"></script>
<script src="js/index.js"></script>

<script>
   /* if($("#comm_user_email").val() == "")
    {
        log_in();
    }
    else
    {

    }*/
</script>
</body>


</html>