/**
 * Created by Sourav on 8/10/2017.
 */

const LED1_ON="led1on\n\r";
const LED2_ON="led2on\n\r";
const LED3_ON="led3on\n\r";
const LED4_ON="led4on\n\r";

const LED1_OFF="led1off\n\r";
const LED2_OFF="led2off\n\r";
const LED3_OFF="led3off\n\r";
const LED4_OFF="led4off\n\r";

const ACLOAD_ON="acon\n\r";
const ACLOAD_OFF="acoff\n\r";

const GET_TEMP="gettemp\n\r";
const GET_STATUS="getstatus\n\r";
const ERROR="error";
const SUCCESS="success";