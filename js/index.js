
function loadmenu() {
    $("#sicons").show("slow");
    $("#main-menu").show("slow");
}

$(document).ready(function () {
    $('#nav-icon1').click(function () {
        $(this).toggleClass('open');
        $(".main-menu").slideToggle(500);
    });
    $('#nav-icon2').click(function () {
        $(this).toggleClass('open');
        $(".main-menu-small").toggle(600);
        $(".small-submenu-ul").hide();
        $(".small-menu-ul").show();
    });
});
/*for fade top header end here*/

/*for small devices header list hide show start here*/
function small_submenu(ulClass) {
    $(".small-menu-ul").hide(500);
    $(".small_sub_" + ulClass).show(500);
}

/*show main menu list start here*/

/*show main menu list end here*/
function small_menu(ulClass) {
    $(".small-menu-ul").show(500);
    $(".small_sub_" + ulClass).hide(500);
}

/*for small devices header list hide show end   here*/



/**
 * Created by Sourav on 8/3/2017.
 */

    $('.nav li').on('click', function(){
        $('.nav li').removeClass('active');
        $(this).addClass('active');
    });

    window.onload = function () {

        $('#led_1').on('click',function () {
           console.log('background --- '+colorToHex( $(this).css('background-color') ));

           if(colorToHex( $(this).css('background-color') ) == "#808080"){
               sendCommand(LED1_OFF);
               $(this).css('background-color','lightgray');
               $("#led_11").removeClass('active');
           }
           else{
               $(this).css('background-color','grey');
               $("#led_11").addClass('active');
               sendCommand(LED1_ON);
           }
        });

        $('#led_2').on('click',function () {
            console.log('background --- '+colorToHex( $(this).css('background-color') ));

            if(colorToHex( $(this).css('background-color') ) == "#808080"){
                sendCommand(LED2_OFF);
                $(this).css('background-color','lightgray');
                $("#led_22").removeClass('active');
            }
            else{
                $(this).css('background-color','grey');
                $("#led_22").addClass('active');
                sendCommand(LED2_ON);
            }
        });

        $('#led_3').on('click',function () {
            console.log('background --- '+colorToHex( $(this).css('background-color') ));

            if(colorToHex( $(this).css('background-color') ) == "#808080"){
                sendCommand(LED3_OFF);
                $(this).css('background-color','lightgray');
                $("#led_33").removeClass('active');
            }
            else{
                $(this).css('background-color','grey');
                $("#led_33").addClass('active');
                sendCommand(LED3_ON);
            }
        });

        $('#led_4').on('click',function () {
            console.log('background --- '+colorToHex( $(this).css('background-color') ));

            if(colorToHex( $(this).css('background-color') ) == "#808080"){
                sendCommand(LED4_OFF);
                $(this).css('background-color','lightgray');
                $("#led_44").removeClass('active');
            }
            else{
                $(this).css('background-color','grey');
                $("#led_44").addClass('active');
                sendCommand(LED4_ON);
            }
        });

        $('.acload').on('click',function () {
            console.log('background --- '+colorToHex( $(this).css('background-color') ));

            if(colorToHex( $(this).css('background-color') ) == "#ab1522"){
                sendCommand(ACLOAD_OFF);
                $(this).css('background-color','lightgrey');
                $(this).css('color','black');

            }
            else{
                $(this).css('background-color','#ab1522');
                $(this).css('color','white');
                sendCommand(ACLOAD_ON);
            }
        });



        sendCommand(GET_STATUS);

        var query = database.ref().child("msg");
        query.on("child_changed",function(data) {
            // var response = "LED STATUS:\"0,1,1,1\"\r\nAC:1\r\nTEMP:27.5\r\n";
            var response = data.val();
            console.log('data----'+response);
            var commands = response.split("\r\n");
            console.log('cmd arr '+commands);

            for(var i=0;i<commands.length;i++) {
                var cmd_name = commands[i].split(":");
                if (cmd_name[0] === "TEMP") {
                    $("#temp_value").html(cmd_name[1]);
                }
                else if (cmd_name[0] === "LED STATUS") {
                    var status = cmd_name[1].trim();
                    status = status.replace(/["']/g, "");
                    var statusarr = status.split(",");

                    if (statusarr.length != 4) {
                        return false;
                    }
                    console.log('status arr -- '+statusarr[3].trim());

                    switch (statusarr[0]) {
                        case "1":
                            $("#led_img1").attr('src', 'images/pic_bulbon.png');
                            break;
                        case "0":
                            $("#led_img1").attr('src', 'images/pic_bulboff.gif');
                            break;
                    }

                    switch (statusarr[1]) {
                        case "1":
                            $("#led_img2").attr('src', 'images/pic_bulbon.png');
                            break;
                        case "0":
                            $("#led_img2").attr('src', 'images/pic_bulboff.gif');
                            break;
                    }

                    switch (statusarr[2]) {
                        case "1":
                            $("#led_img3").attr('src', 'images/pic_bulbon.png');
                            break;
                        case "0":
                            $("#led_img3").attr('src', 'images/pic_bulboff.gif');
                            break;
                    }

                    switch (statusarr[3]) {
                        case "1":
                            $("#led_img4").attr('src', 'images/pic_bulbon.png');
                            break;
                        case "0":
                            $("#led_img4").attr('src', 'images/pic_bulboff.gif');
                            break;
                    }
                }
                else if(cmd_name[0] === "LED1") {
                    switch (cmd_name[1].trim()) {
                        case "ON":
                            $("#led_img1").attr('src', 'images/pic_bulbon.png');
                            break;
                        case "OFF":
                            $("#led_img1").attr('src', 'images/pic_bulboff.gif');
                            break;
                    }
                }
                else if(cmd_name[0] === "LED2") {
                    switch (cmd_name[1].trim()) {
                        case "ON":
                            $("#led_img2").attr('src', 'images/pic_bulbon.png');
                            break;
                        case "OFF":
                            $("#led_img2").attr('src', 'images/pic_bulboff.gif');
                            break;
                    }
                }
                else if(cmd_name[0] === "LED3") {
                    switch (cmd_name[1].trim()) {
                        case "ON":
                            $("#led_img3").attr('src', 'images/pic_bulbon.png');
                            break;
                        case "OFF":
                            $("#led_img3").attr('src', 'images/pic_bulboff.gif');
                            break;
                    }
                }
                else if(cmd_name[0] === "LED4") {
                    switch (cmd_name[1].trim()) {
                        case "ON":
                            $("#led_img4").attr('src', 'images/pic_bulbon.png');
                            break;
                        case "OFF":
                            $("#led_img4").attr('src', 'images/pic_bulboff.gif');
                            break;
                    }
                }
                else if(cmd_name[0] === "AC") {
                    switch (cmd_name[1].trim()) {
                        case "1":
                        case "ON":
                        case "ON\n\r":
                            $(".acload").css('background-color','#ab1522');
                            $(".acload").css('color','white');
                         break;
                        case "OFF":
                        case "OFF\n\r":
                        case "0":
                            $(".acload").css('background-color','lightgray');
                            $(".acload").css('color','black');
                            break;
                    }
                }

            }

        });

    };

    function sendCommand(cmd) {
        console.log('getstatus is called---');

        var newworkref = database.ref().child('msg');
        newworkref.set({'message':cmd});
    }

  function colorToHex(color) {
    if (color.substr(0, 1) === '#') {
        return color;
    }
    var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color);

    var red = parseInt(digits[2]);
    var green = parseInt(digits[3]);
    var blue = parseInt(digits[4]);

    var rgb = blue | (green << 8) | (red << 16);
    return digits[1] + '#' + rgb.toString(16);
 };



/*function changeImage(id) {
    var image = document.getElementById(id);
    if (image.src.match("bulbon")) {
        image.src = "images/pic_bulboff.gif";
    } else {
        image.src = "images/pic_bulbon.gif";

    }
}*/

$(".led_img").click(function () {
   var id = this.id;
   var image = document.getElementById(id);
   var ledOnOff = "";
   switch (id){
       case "led_img1":
           if (image.src.match("bulbon")) {
               image.src = "images/pic_bulboff.gif";
               sendCommand(LED1_OFF);
           } else {
               image.src = "images/pic_bulbon.png";
               sendCommand(LED1_ON);
           }
           break;
       case "led_img2":
           if (image.src.match("bulbon")) {
               image.src = "images/pic_bulboff.gif";
               sendCommand(LED2_OFF);
           } else {
               image.src = "images/pic_bulbon.png";
               sendCommand(LED2_ON);
           }
           break;
       case "led_img3":
           if (image.src.match("bulbon")) {
               image.src = "images/pic_bulboff.gif";
               sendCommand(LED3_OFF);
           } else {
               image.src = "images/pic_bulbon.png";
               sendCommand(LED3_ON);
           }
           break;
       case "led_img4":
           if (image.src.match("bulbon")) {
               image.src = "images/pic_bulboff.gif";
               sendCommand(LED4_OFF);
           } else {
               image.src = "images/pic_bulbon.png";
               sendCommand(LED4_ON);
           }
           break;
   }
});

/*login start here*/
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function log_in() {
    $("#login_modal").modal("show");
    $(".modal-body").html('<div class="row"><div class="col-md-12 login_box"> <p class="login_error"></p><label class="login_text_field"> Enter User Name ' +
        '</label><input type="text" class="form-control login_field login_username" placeholder="User Name...">' +
        '</div><div class="col-md-12 login_box"><label class="login_text_field">Enter Password</label><input type="password" class="form-control ' +
        'login_field login_password" placeholder="Password...">' +
        '</div> <div class="col-md-12 login_box"><input type="button" onclick="login()"  class="login_button pull-right" value="Login"> </div></div>');
}
function login() {
    var login_username = $(".login_username").val();
    var login_password = $(".login_password").val();
    if(login_username == "" || login_password == ""){
        $(".login_error").html("All fields are required.");
    }
    else
    {
        if(validateEmail(login_username))
        {
            var url = "comm_login.php?type=login&login_username=" + login_username + "&login_password=" + login_password;
            $.get(url, function (data) {
                var jsonData    = $.parseJSON(data);
                var status = jsonData.status;
                var Message = jsonData.message;
                if (status == "Success") {
                   window.location="index.php";
                }
                else {
                    $(".login_error").html(Message);
                }
            });
        }
        else
        {
            $(".login_error").html("Invalid email-id");
        }
    }
}
/*login end here*/
