<?php
session_start();
error_reporting(0);
$comm_useremail ="";
if(isset($_SESSION['comm_email_id']))
{
    $comm_useremail = $_SESSION['comm_email_id'];
}
else
{
    header("Location:signin.php");
}
echo "<input type='hidden' value='$comm_useremail' id='comm_user_email'>";
include "header.php";
 ?>
<div class="container">
    <div class="col-md-12 col-xs-12 no-gutter" >
        <div class="col-md-12">
            <ul class="list-unstyled list-inline set_top_ul">
                <li class="pull-left"><h2>Setting </h2></li>
                <li class="pull-right">
                    <button class="btn btn-md btn-primary " style="margin-top: 20px;">Save</button></li>
            </ul>
        </div>
    </div>
    <div class="col-md-12 main-content">
        <div class="col-md-12 no-gutter data-set ">
            <div class="col-md-12 no-gutter">
                <label style="font-size: 23px;">Connect To Server</label>
            </div>
            <div class="col-md-12 no-gutter">
                <ul class="list-inline list-unstyled">
                    <li ><label style="color: grey;font-size: 20px" >Server Connection</label></li>
                    <li><input type="checkbox" /></li>
                </ul>
            </div>
            <div class="col-md-12 no-gutter">
                <ul class="list-unstyled">
                    <li ><label style="color: grey;" >Server/WIFI IP :</label></li>
                    <li ><input type="text" class="form-control" placeholder="Enter IP address" style="width: 80%"/></li>
                </ul>
            </div>
            <div class="col-md-12 no-gutter">
                <ul class="list-unstyled">
                    <li ><label style="color: grey;" >Port :</label></li>
                    <li ><input type="number" class="form-control" placeholder="Enter Port" style="width: 80%"/></li>
                </ul>
            </div>
        </div>

        <div class="col-md-12 no-gutter data-set " style="margin-top: 2%">
            <div class="col-md-12 no-gutter">
                <label style="font-size: 23px;">Advanced Setting</label>
            </div>
            <div class="col-md-12 no-gutter">
                <ul class="list-unstyled">
                    <li ><label style="color: grey;" >Connection Retry Timeout (5-180)s</label></li>
                    <li ><input type="number" class="form-control" placeholder="Enter retry timeout" style="width: 80%"/></li>
                </ul>
            </div>
            <div class="col-md-12 no-gutter">
                <ul class="list-inline list-unstyled">
                    <li ><label style="color: grey;font-size: 20px" >Send Auto Command</label></li>
                    <li ><input type="checkbox" /></li>
                </ul>
            </div>
            <div class="col-md-12 no-gutter">
                <ul class="list-unstyled">
                    <li ><label style="color: grey;" >Auto Command Send Interval (5-30)s</label></li>
                    <li ><input type="number" class="form-control" placeholder="Enter Interval" style="width: 80%"/></li>
                </ul>
            </div>
        </div>

    </div>
</div>

 <?php
  include 'footer.php';
 ?>

<script>
    $('#setting').click();
    $('.navbar-brand').removeClass('active');
</script>
