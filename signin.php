<?php
include 'header.php';
?>
<div class="container">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="row">
            <h1 class="login_text">
                Login
            </h1>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 login_box">
                <p class="login_error"></p>
                <label class="login_text_field"> Enter User Name</label>
                <input type="text" class="form-control login_field login_username" placeholder="User Name...">
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 login_box">
                <label class="login_text_field">Enter Password</label>
                <input type="password"   class="form-control login_field login_password" placeholder="Password...">
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 login_box">
                <input type="button" onclick="login()" class="login_button pull-right"   value="Login">
            </div>
        </div>

    </div>
    <div class="col-md-2"></div>

</div>


<?php
include "footer.php";
?>